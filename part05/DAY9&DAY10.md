## BADGE组件

首先需要限定badge颜色，颜色分为字体颜色和底座颜色，为了都可以配置，我们分成2个对象编写：

```javascript
export const badgeColor = {
	positive: color.positive,
	negative: color.negative,
	neutral: color.dark,
	warning: color.warning,
	error: color.lightest,
};

export const badgeBackground = {
	positive: background.positive,
	negative: background.negative,
	neutral: color.mediumlight,
	warning: background.warning,
	error: color.negative,
};
```

组件方面直接传入即可：

```typescript
const BadgeWrapper = styled.div<BadgeProps>`
	display: inline-block;
	vertical-align: top;
	font-size: 11px;
	line-height: 12px;
	padding: 4px 12px;
	border-radius: 3em;
	font-weight: ${typography.weight.bold};

	svg {
		height: 12px;
		width: 12px;
		margin-right: 4px;
		margin-top: -2px;
	}

	${(props) =>
		props.status === "positive" &&
		css`
			color: ${badgeColor.positive};
			background: ${badgeBackground.positive};
		`};

	${(props) =>
		props.status === "negative" &&
		css`
			color: ${badgeColor.negative};
			background: ${badgeBackground.negative};
		`};

	${(props) =>
		props.status === "warning" &&
		css`
			color: ${badgeColor.warning};
			background: ${badgeBackground.warning};
		`};

	${(props) =>
		props.status === "error" &&
		css`
			color: ${badgeColor.error};
			background: ${badgeBackground.error};
		`};

	${(props) =>
		props.status === "neutral" &&
		css`
			color: ${badgeColor.neutral};
			background: ${badgeBackground.neutral};
		`};
`;


export interface BadgeProps extends HTMLAttributes<HTMLDivElement> {
	/** 状态色*/
	status?: "positive" | "negative" | "neutral" | "warning" | "error";
}

export function Badge(props: PropsWithChildren<BadgeProps>) {
	return <BadgeWrapper {...props} />;
}
Badge.defaultProps = {
	status: "neutral",
};
export default Badge;
```

其实不需要default导出，但是由于dockgen识别ts的bug，所以需要这么做。status那里也得手写，不然识别不出来。

下面编写story，老样子，第一个还是写个knob badge：

```typescript
type selectType = "positive" | "negative" | "neutral" | "warning" | "error";

export const knobsBadge = () => (
	<Badge
		status={select<BadgeProps["status"]>(
			"status",
			Object.keys(badgeColor) as selectType[],
			"neutral"
		)}
	>
		{text("children", "i am badge")}
	</Badge>
);
```

后面story进行展示：

```typescript
export const all = () => (
	<div>
		<Badge status="positive">Positive</Badge>
		<Badge status="negative">Negative</Badge>
		<Badge status="neutral">Neutral</Badge>
		<Badge status="error">Error</Badge>
		<Badge status="warning">Warning</Badge>
	</div>
);

export const withIcon = () => (
	<Badge status="warning">
		<Icon icon="check" />
		with icon
	</Badge>
);
```

编写测试：

```typescript
import React from "react";
import { render, fireEvent, cleanup } from "@testing-library/react";
import { Badge, badgeBackground, badgeColor, BadgeProps } from "../index";

const testonClick = jest.fn();

const testThemeFunc = (status: BadgeProps["status"]) => {
	cleanup();
	let wrapper = render(<Badge status={status}>111</Badge>);
	const text = wrapper.getByText("111");
	expect(text).toHaveStyle(`color: ${badgeColor[status!]}`);
	expect(text).toHaveStyle(`background: ${badgeBackground[status!]}`);
};

describe("test Badge component", () => {
	it("should render default style", () => {
		let wrapper = render(<Badge>111</Badge>);
		expect(wrapper).toMatchSnapshot();
		const text = wrapper.getByText("111");
		expect(text).toHaveStyle(`color: ${badgeColor.neutral}`);
		expect(text).toHaveStyle(`background: ${badgeBackground.neutral}`);
	});
	it("should render correct  attr", () => {
		let wrapper = render(
			<Badge className="testclass" onClick={testonClick}>
				attr
			</Badge>
		);
		const text = wrapper.getByText("attr");
		expect(text.className.includes("testclass")).toBeTruthy();
		fireEvent.click(text);
		expect(testonClick).toHaveBeenCalled();
	});
	it("should rende correct theme", () => {
		testThemeFunc("positive");
		testThemeFunc("warning");
		testThemeFunc("negative");
		testThemeFunc("neutral");
		testThemeFunc("error");
	});
});
```

## Radio

radio是单选框，它跟checkbox有个区别就是radio不好选中取消。 

### 组件需求

先分析下组件需求：

一个radio，还需要有个label，label有个特性就是点它可以点中对应的单选。

radio还有个特性，就是name相同的radio是可以切换的。这是天生自带的。

label还应该可以隐藏，隐藏后只显示选框。

对于disabled，需要设置下样式。

同样的，还应该有主题颜色。

label下方和右方还可以配置些文本。

差不多就是这些。

### 编写radio

这里styledcomponent的属性和allhtmlAttributes的属性冲突，所以要忽略其as属性。另外label因为all里面有这属性，而且是字符串类型，但是我们需要让label支持icon，所以也要忽略它，将label作为reactNode类型。

```typescript
import React, { ReactNode, AllHTMLAttributes } from "react";
import styled, { css } from "styled-components";
import { color, typography } from "../shared/styles";
import { rgba } from "polished";

const Label = styled.label<RadioProps>`
	cursor: ${(props) => (props.disabled ? "not-allowed" : "pointer")};
	font-size: ${typography.size.s2}px;
	font-weight: ${typography.weight.bold};
	position: relative;
	height: 1em;
	display: flex;
	align-items: center;
	opacity: ${(props) => (props.disabled ? 0.5 : 1)};
`;

const OptionalText = styled.span<RadioProps>`
	${(props) =>
		props.hideLabel &&
		css`
			border: 0px !important;
			clip: rect(0 0 0 0) !important;
			-webkit-clip-path: inset(100%) !important;
			clip-path: inset(100%) !important;
			height: 1px !important;
			overflow: hidden !important;
			padding: 0px !important;
			position: absolute !important;
			white-space: nowrap !important;
			width: 1px !important;
		`}
`;

const Description = styled.div`
	font-size: ${typography.size.s1}px;
	font-weight: ${typography.weight.regular};
	color: ${color.mediumdark};
	margin-top: 4px;
	margin-left: calc(${typography.size.s2}px + 0.4em);
	width: 100%;
`;

const RadioWrapper = styled.div`
	display: flex;
	align-items: center;
	flex-wrap: wrap;
`;

const Input = styled.input<RadioProps>`
	margin: 0 0.4em 0 0;
	font-size: initial;
	opacity: 0;

	& + span {
		&:before,
		&:after {
			position: absolute;
			top: 0;
			left: 0;
			height: 1em;
			width: 1em;
			content: "";
			display: block;
			border-radius: 3em;
		}
	}

	& + span:before {
		box-shadow: ${color.mediumdark} 0 0 0 1px inset;
	}

	&:focus + span:before {
		box-shadow: ${(props) => color[props.appearance!]} 0 0 0 1px inset;
	}

	&:checked + span:before {
		box-shadow: ${(props) => color[props.appearance!]} 0 0 0 1px inset;
	}

	&:checked:focus + span:before {
		box-shadow: ${(props) => color[props.appearance!]} 0 0 0 1px inset,
			${(props) => rgba(color[props.appearance!], 0.3)} 0 0 5px 2px;
	}

	& + span:after {
		transition: all 150ms ease-out;
		transform: scale3d(0, 0, 1);

		height: 10px;
		margin-left: 2px;
		margin-top: 2px;
		width: 10px;

		opacity: 0;
	}

	&:checked + span:after {
		transform: scale3d(1, 1, 1);
		background: ${(props) => color[props.appearance!]};
		opacity: 1;
	}
`;

const Error = styled.span`
	font-weight: ${typography.weight.regular};
	font-size: ${typography.size.s2}px;
	color: ${color.negative};
	margin-left: 6px;
	height: 1em;
	display: flex;
	align-items: center;
`;

export interface RadioProps
	extends Omit<AllHTMLAttributes<HTMLInputElement>, "as" | "label"> {
	/** 主题色 */
	appearance?: keyof typeof color;
	/** label展示 */
	label?: ReactNode;
	/** 是否隐藏label*/
	hideLabel?: boolean;
	/** 错误文本 */
	error?: string;
	/** 描述文本 */
	description?: string;
	/** wrapper类名 */
	wrapperClass?: string;
}

export function Radio(props: RadioProps) {
	const {
		wrapperClass,
		error,
		description,
		label,
		hideLabel,
		style,
		...restProps
	} = props;
	const { disabled } = props;

	return (
		<RadioWrapper className={wrapperClass} style={style}>
			<Label disabled={disabled}>
				<Input
					{...restProps}
					role="radio"
					aria-invalid={!!error}
					type="radio"
				/>
				<span>
					<OptionalText hideLabel={hideLabel}>{label}</OptionalText>
				</span>
			</Label>
			{error && <Error>{error}</Error>}
			{description && <Description>{description}</Description>}
		</RadioWrapper>
	);
}

Radio.defaultProps = {
	appearance: "primary",
	hideLabel: false,
};

export default Radio;
```

### 编写Story

story方面第一个做个knob可控制的，值得注意的就是checked属性如果控制的话，是不受外界点击影响的。

对于颜色story，直接把color拿来循环下。

最后一个story则是父组件进行控制radio组件例子。

```typescript
import React, { useState } from "react";
import { Radio } from "./index";
import { withKnobs, text, boolean, select } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { color } from "../shared/styles";
import { Icon } from "../icon";

export default {
	title: "Radio",
	component: Radio,
	decorators: [withKnobs],
};

export const knobsRadio = () => (
	<Radio
		appearance={select<keyof typeof color>(
			"color",
			Object.keys(color) as Array<keyof typeof color>,
			"primary"
		)}
		label={text("label", "i am radio")}
		onChange={onChange}
		hideLabel={boolean("hideLabel", false)}
		error={text("error", "")}
		description={text("description", "")}
		disabled={boolean("disabled", false)}
	></Radio>
);

export const testColors = () => (
	<div>
		{Object.keys(color).map((v, i) => (
			<Radio
				key={i}
				name="group2"
				label={v}
				appearance={v as keyof typeof color}
			/>
		))}
	</div>
);

const onChange = action("change");

export const testOnchange = () => (
	<form>
		<Radio name="group1" label="apple" onChange={onChange} />
		<Radio name="group1" label="banana" onChange={onChange} />
		<Radio name="group1" label="pear" onChange={onChange} />
		<Radio name="group1" label="mongo" onChange={onChange} />
		<Radio name="group1" label="watermelon" onChange={onChange} />
	</form>
);

export const testDisabled = () => <Radio label="disabled" disabled></Radio>;

export const testExtraText = () => (
	<Radio
		label="the radio has extra text"
		error="error text"
		description="description text"
	></Radio>
);

export const testHideLabel = () => (
	<Radio
		label="the radio has extra text"
		description="label will hidden"
		hideLabel
	></Radio>
);

export const withIcon = () => (
	<Radio
		label={
			<span>
				<Icon icon="redux"></Icon>with icon
			</span>
		}
	></Radio>
);

function ParentControl() {
	const [state, setState] = useState(() => new Array(5).fill(false));
const onClick = (e: React.MouseEvent<HTMLInputElement, MouseEvent>) => {
		const target = e.target as HTMLInputElement;
		const index = (target.value as unknown) as number;
		let newArr = new Array(5).fill(false);
		newArr[index] = true;
		setState(newArr);
	};
	return (
		<div>
			<Radio
				label="apple"
				onClick={onClick}
				value={0}
				checked={state[0]}
				onChange={() => {}}
			/>
			<Radio
				label="banana"
				onClick={onClick}
				value={1}
				checked={state[1]}
				onChange={() => {}}
			/>
			<Radio
				label="pear"
				onClick={onClick}
				value={2}
				checked={state[2]}
				onChange={() => {}}
			/>
			<Radio
				label="mongo"
				onClick={onClick}
				value={3}
				checked={state[3]}
				onChange={() => {}}
			/>
			<Radio
				label="watermelon"
				onClick={onClick}
				value={4}
				checked={state[4]}
				onChange={() => {}}
			/>
		</div>
	);
}

export const testParentControl = () => <ParentControl></ParentControl>;
```



### 编写测试

直接用代码不好测试样式，因为radio组件的选中那玩意是2个伪类做的，选择器不好选中它，直接用快照生成，其他属性挨个测试，最后达到100%就可以了。

```typescript
import React from "react";
import { render, fireEvent } from "@testing-library/react";
import { Radio } from "../index";
import { color } from "../../shared/styles";

const testfn = jest.fn();
const disablefn = jest.fn();
describe("test Radio component", () => {
	it("should checked when clicked", () => {
		const wrapper = render(<Radio label="test" onChange={testfn}></Radio>);
		expect(wrapper).toMatchSnapshot();
		const input = wrapper.container.querySelector("input")!;
		expect(testfn).not.toHaveBeenCalled();
		fireEvent.click(input);
		expect(testfn).toHaveBeenCalled();
	});
	it("should render extra text", () => {
		const wrapper = render(
			<Radio
				label="test"
				error="errortext"
				description="description"
				onChange={testfn}
			></Radio>
		);
		expect(wrapper).toMatchSnapshot();
		const errortext = wrapper.getByText("errortext");
		expect(errortext).toHaveStyle(`color:${color.negative}`);
		const description = wrapper.getByText("description");
		expect(description).toHaveStyle(`color:${color.mediumdark}`);
	});
	it("should hide label", () => {
		const wrapper = render(<Radio label="test" hideLabel></Radio>);
		expect(wrapper).toMatchSnapshot();
		const text = wrapper.getByText("test");
		expect(text).toHaveStyle("clip-path: inset(100%)");
	});
	it("should disabled", () => {
		const wrapper = render(
			<Radio label="test" disabled onChange={disablefn}></Radio>
		);
		expect(wrapper).toMatchSnapshot();
		const text = wrapper.getByText("test");
		fireEvent.click(text);
		expect(disablefn).not.toHaveBeenCalled();
	});
});

```



## 今日作业

完成badge与radio组件

### 可选作业

前面我们已经走了一次打包发布流程了，但这个包并不完善，一般情况除了esmodule，还要打包别的模块的。 

### 使用MICROBUNDLE 

microbundle是国外比较火的零配置打包工具，它旨在只使用package.json配置完成复杂的打包需求。

官网：https://github.com/developit/microbundle

首先-D安装它。 

microbundle推荐ts配置里module和target为esnext。我们进行更改下：

```
"target": "ESNext",
"module": "ESNext"
```

 然后在package.json里修改build指令 

```
"build": "microbundle build  --tsconfig tsconfig.build.json --jsx React.createElement"
```

然后运行它，dist文件里就已经生成打包好的文件了。

默认打包生成4种格式modern，esm，cjs，umd，这玩意有个bug，就是esm和cjs名字一样，会导致覆盖。所以需要分开打包，打包完成后，使用cjs打包进行覆盖。 

修改指令： 

```
 "build-cjs": "microbundle build --tsconfig tsconfig.build.json --jsx React.createElement  --compress=false --format cjs", "build-all": "microbundle build --tsconfig tsconfig.build.json --jsx React.createElement  --compress=false", "build": "npm run build-all & npm run build-cjs", 
```

运行build重新打包，可以发现模块就齐了。

然后需要在Package.json配置模块： 

```
	"main": "dist/atom-design-explorer.js",
	"module": "dist/atom-design-explorer.modern.js",
```

### 测试包

我们换一个文件夹cra新开个项目，安装刚刚发布的包。

使用以下demo：

```typescript
import React from "react";
import { Button, Icon } from "@yehuozhili/atom-design-explorer";
import { GlobalStyle, color } from "@yehuozhili/atom-design-explorer";
color.primary = "blue";

function App() {
	return (
		<div className="App">
			<GlobalStyle></GlobalStyle>
			<Button appearance="primary">2222</Button>
			<Icon icon="admin"></Icon>
		</div>
	);
}

export default App;

```

可以如果启动后能看见蓝色的button就说明成功了。

生产环境测试：

使用npm run build进行打包。

使用`npx http-server ./build`查看输出文件，如果也是同样效果证明没有问题。

### 目录顺序

preview里可以设置目录顺序：

```javascript
import { addParameters, configure } from "@storybook/react";

const loaderFn = () => {
	return [
		require("../src/stories/Welcome.stories.mdx"),
		require("../src/stories/color.stories.mdx"),
		....
		....
		....
		...
	];
};
configure(loaderFn, module);
```

