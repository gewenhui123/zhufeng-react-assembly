## 本节效果
本节视频 http://img.zhufengpeixun.cn/2.reactui_part2.mp4

目录结构
```
├── package.json
├── public
│   └── index.html
├── README.md
├── src
│   ├── components
│   │   ├── button
│   │   │   ├── button.stories.tsx
│   │   │   ├── index.tsx
│   │   │   └── __test__
│   │   │       └── button.test.tsx
│   │   └── shared
│   │       ├── animation.tsx
│   │       ├── global.tsx
│   │       ├── styles.tsx
│   │       └── __test__
│   │           ├── global.test.tsx
│   │           └── __snapshots__
│   │               └── global.test.tsx.snap
│   ├── react-app-env.d.ts
│   ├── setupTests.ts
│   └── stories
│       ├── Color.stories.mdx
│       ├── Typography.stories.mdx
│       └── Welcome.stories.mdx
├── tsconfig.json
└── yarn.lock
```


## 编写Button组件

今天起我们要制作第一个组件button。

### 需求分析

首先需要分析下button的需求。

首要需求就是颜色，昨天已经对颜色制作了token，组件只要根据传参来显示对应颜色即可。同时，颜色除了背景颜色，还有文字颜色，边框颜色。

然后是大小，我们需要进行传参，然后改变button的大小，可能还需要处理里面文字大小。

此外，点击跳转也是个常用功能，如果传递href的话，需要渲染a标签而不是button标签。

loading状态， 这个需要处理很多细节方面。

disabled状态，这也是很常用的功能，需要单独设置disabled状态和样式。

### 文件结构与代码组织

styled-components制作组件的方法跟传统用css或者预处理器制作的模式有点不太一样。

传统做法是建立样式文件写对应类名，js中控制改变类名即可。

styled-components可以理解成就是写行内样式，其实是js插个动态类把样式弄来。先将各种属性以及对应传参写出来，产生对应样式，再把这个组件渲染出来即可。

对于每个组件，可以一个组件对应一个文件夹，以及其story文件和test文件。复杂组件可能需要建立多个文件，这里为了方便，将button组件直接写到button下的index里。



#### polished

为了在styled-components里像scss等预处理器一样写常用函数，还需要安装下polished这个库。

网站官网https://polished.js.org/docs/

```js
$ npm install --save polished
```

-S安装它，它自带声明，不需要安装@types

#### 定义类型

首先，先定义button的表现形式，这里使用几种类型，主题色，主题反白色，二级主题色，二级主题反白色，默认轮廓，轮廓主题色，轮廓二级主题色，三级主题色。

同时，使用字面量类型对传参进行约束。这里可能有人奇怪为什么写2次类型，因为ts的字面量类型与string类型转换有bug，默认会先去匹配string类型而非字面量类型，并且，类型无法进行循环引用，所以需要单独写出来再定义上。
修改文件
src\components\button\index.tsx

```typescript
export type AppearancesTypes = keyof typeof APPEARANCES;

type btnType =
	| "primary"
	| "primaryOutline"
	| "secondary"
	| "secondaryOutline"
	| "tertiary"
	| "outline"
	| "inversePrimary"
	| "inverseSecondary"
	| "inverseOutline";

type AppearancesObj = {
	[key in btnType]: btnType;
};

export const APPEARANCES: AppearancesObj = {
	primary: "primary",
	primaryOutline: "primaryOutline",
	secondary: "secondary",
	secondaryOutline: "secondaryOutline",
	tertiary: "tertiary",
	outline: "outline",
	inversePrimary: "inversePrimary",
	inverseSecondary: "inverseSecondary",
	inverseOutline: "inverseOutline",
};
```

 同理，size也这么操作。

```typescript
export type SizesTypes = keyof typeof SIZES;
type sizeType = "small" | "medium";
type sizeObj = {
	[key in sizeType]: sizeType;
};
export const SIZES: sizeObj = {
	small: "small",
	medium: "medium",
};
```

编写button内部的span以及loading样式：

```javascript
const Text = styled.span`
	display: inline-block;
	vertical-align: top;
`;

const Loading = styled.span`
	position: absolute;
	top: 50%;
	left: 0;
	right: 0;
	opacity: 0;
`;
export const btnPadding = {
  medium: "13px 20px",
  small: "8px 16px",
};
```

对传入接口进行定义：

```javascript
export interface CustormButtonProps {
	/** 是否禁用 */
	disabled?: boolean;
	/** 是否加载中 */
	isLoading?: boolean;
	/** 是否是a标签 */
	isLink?: boolean;
	/** 是否替换加载中文本 */
	loadingText?: ReactNode;
	/** 按钮大小 */
	size?: SizesTypes;
	/** 按钮类型 */
	appearance?: AppearancesTypes;
	/** 无效点击 */
	isUnclickable?: boolean;
}

export type ButtonProps = CustormButtonProps &
	AnchorHTMLAttributes<HTMLAnchorElement> &
	ButtonHTMLAttributes<HTMLButtonElement>;

```

自定义属性必须导出，必须按此格式写注释，这种写法会被dockgen插件捕获从而显示到属性页面上。



#### 编写样式

对于贝塞尔曲线之类的可以单独拎出来放到shared的animation.tsx里。

shared/animation.tsx

```typescript
export const easing = {
	rubber: "cubic-bezier(0.175, 0.885, 0.335, 1.05)",
};
```

将所有东西导入`src\components\button\index.tsx`编写样式：

```javascript
import styled from "styled-components";
import { color, typography } from "../shared/styles";
import { darken, rgba, opacify } from "polished";
import {easing}from '../shared/animation'
```

制作需要渲染的button样式，这段有点长，各位可以自行发挥：

```typescript
const StyledButton = styled.button<ButtonProps>`
  border: 0;
  border-radius: 3em;
  cursor: pointer;
  display: inline-block;
  overflow: hidden;
  padding: ${(props) =>
		props.size === SIZES.small ? "8px 16px" : "13px 20px"};
  position: relative;
  text-align: center;
  text-decoration: none;
  transition: all 150ms ease-out;
  transform: translate3d(0,0,0);
  vertical-align: top;
  white-space: nowrap;
  user-select: none;
  opacity: 1;
  margin: 0;
  background: transparent;


  font-size: ${(props) =>
		props.size === SIZES.small ? typography.size.s1 : typography.size.s2}px;
  font-weight: ${typography.weight.extrabold};
  line-height: 1;

  ${(props) =>
		!props.isLoading &&
		`
      &:hover {
        transform: translate3d(0, -2px, 0);
        box-shadow: rgba(0, 0, 0, 0.2) 0 2px 6px 0;
      }

      &:active {
        transform: translate3d(0, 0, 0);
      }

      &:focus {
        box-shadow: ${rgba(color.primary, 0.4)} 0 1px 9px 2px;
      }

      &:focus:hover {
        box-shadow: ${rgba(color.primary, 0.2)} 0 8px 18px 0px;
      }
    `}

  ${Text} {
    transform: scale3d(1,1,1) translate3d(0,0,0);
    transition: transform 700ms ${easing.rubber};
    opacity: 1;
  }

  ${Loading} {
    transform: translate3d(0, 100%, 0);
  }

  ${(props) =>
		props.disabled &&
		`
      cursor: not-allowed !important;
      opacity: 0.5;
      &:hover {
        transform: none;
      }
    `}

  ${(props) =>
		props.isUnclickable &&
		`
      cursor: default !important;
      pointer-events: none;
      &:hover {
        transform: none;
      }
    `}

  ${(props) =>
		props.isLoading &&
		`
      cursor: progress !important;
      opacity: 0.7;

      ${Loading} {
        transition: transform 700ms ${easing.rubber};
        transform: translate3d(0, -50%, 0);
        opacity: 1;
      }

      ${Text} {
        transform: scale3d(0, 0, 1) translate3d(0, -100%, 0);
        opacity: 0;
      }

      &:hover {
        transform: none;
      }
    `}



  ${(props) =>
		props.appearance === APPEARANCES.primary &&
		`
      background: ${color.primary};
      color: ${color.lightest};

      ${!props.isLoading &&
			`
          &:hover {
            background: ${darken(0.05, color.primary)};
          }
          &:active {
            box-shadow: rgba(0, 0, 0, 0.1) 0 0 0 3em inset;
          }
          &:focus {
            box-shadow: ${rgba(color.primary, 0.4)} 0 1px 9px 2px;
          }
          &:focus:hover {
            box-shadow: ${rgba(color.primary, 0.2)} 0 8px 18px 0px;
          }
        `}
    `}

  ${(props) =>
		props.appearance === APPEARANCES.secondary &&
		`
      background: ${color.secondary};
      color: ${color.lightest};

      ${!props.isLoading &&
			`
          &:hover {
            background: ${darken(0.05, color.secondary)};
          }
          &:active {
            box-shadow: rgba(0, 0, 0, 0.1) 0 0 0 3em inset;
          }
          &:focus {
            box-shadow: ${rgba(color.secondary, 0.4)} 0 1px 9px 2px;
          }
          &:focus:hover {
            box-shadow: ${rgba(color.secondary, 0.2)} 0 8px 18px 0px;
          }
        `}
    `}

  ${(props) =>
		props.appearance === APPEARANCES.tertiary &&
		`
      background: ${color.tertiary};
      color: ${color.darkest};

      ${!props.isLoading &&
			`
          &:hover {
            background: ${darken(0.05, color.tertiary)};
          }
          &:active {
            box-shadow: rgba(0, 0, 0, 0.1) 0 0 0 3em inset;
          }
          &:focus {
            box-shadow: ${rgba(color.darkest, 0.15)} 0 1px 9px 2px;
          }
          &:focus:hover {
            box-shadow: ${rgba(color.darkest, 0.05)} 0 8px 18px 0px;
          }
        `}
    `}

  ${(props) =>
		props.appearance === APPEARANCES.outline &&
		`
      box-shadow: ${opacify(0.05, color.border)} 0 0 0 1px inset;
      color: ${color.dark};
      background: transparent;

      ${!props.isLoading &&
			`
          &:hover {
            box-shadow: ${opacify(0.3, color.border)} 0 0 0 1px inset;
          }

          &:active {
            background: ${opacify(0.05, color.border)};
            box-shadow: transparent 0 0 0 1px inset;
            color: ${color.darkest};
          }

          &:active:focus:hover {
            ${
				/* This prevents the semi-transparent border from appearing atop the background */ ""
			}
            background: ${opacify(0.05, color.border)};
            box-shadow:  ${rgba(color.darkest, 0.15)} 0 1px 9px 2px;
          }

          &:focus {
            box-shadow: ${opacify(0.05, color.border)} 0 0 0 1px inset, 
            ${rgba(color.darkest, 0.15)} 0 1px 9px 2px;
          }
          &:focus:hover {
            box-shadow: ${opacify(0.05, color.border)} 0 0 0 1px inset, 
            ${rgba(color.darkest, 0.05)} 0 8px 18px 0px;
          }
        `};
    `};

    ${(props) =>
		props.appearance === APPEARANCES.primaryOutline &&
		`
        box-shadow: ${color.primary} 0 0 0 1px inset;
        color: ${color.primary};

        &:hover {
          box-shadow: ${color.primary} 0 0 0 1px inset;
          background: transparent;
        }

        &:active {
          background: ${color.primary};
          box-shadow: ${color.primary} 0 0 0 1px inset;
          color: ${color.lightest};
        }
        &:focus {
          box-shadow: ${color.primary} 0 0 0 1px inset, ${rgba(
			color.primary,
			0.4
		)} 0 1px 9px 2px;
        }
        &:focus:hover {
          box-shadow: ${color.primary} 0 0 0 1px inset, ${rgba(
			color.primary,
			0.2
		)} 0 8px 18px 0px;
        }
      `};

    ${(props) =>
		props.appearance === APPEARANCES.secondaryOutline &&
		`
        box-shadow: ${color.secondary} 0 0 0 1px inset;
        color: ${color.secondary};

        &:hover {
          box-shadow: ${color.secondary} 0 0 0 1px inset;
          background: transparent;
        }

        &:active {
          background: ${color.secondary};
          box-shadow: ${color.secondary} 0 0 0 1px inset;
          color: ${color.lightest};
        }
        &:focus {
          box-shadow: ${color.secondary} 0 0 0 1px inset,
            ${rgba(color.secondary, 0.4)} 0 1px 9px 2px;
        }
        &:focus:hover {
          box-shadow: ${color.secondary} 0 0 0 1px inset,
            ${rgba(color.secondary, 0.2)} 0 8px 18px 0px;
        }
      `};

      ${(props) =>
			props.appearance === APPEARANCES.inversePrimary &&
			`
          background: ${color.lightest};
          color: ${color.primary};

          ${!props.isLoading &&
				`
              &:hover {
                background: ${color.lightest};
              }
              &:active {
                box-shadow: rgba(0, 0, 0, 0.1) 0 0 0 3em inset;
              }
              &:focus {
                box-shadow: ${rgba(color.primary, 0.4)} 0 1px 9px 2px;
              }
              &:focus:hover {
                box-shadow: ${rgba(color.primary, 0.2)} 0 8px 18px 0px;
              }
          `}
      `}

      ${(props) =>
			props.appearance === APPEARANCES.inverseSecondary &&
			`
          background: ${color.lightest};
          color: ${color.secondary};

          ${!props.isLoading &&
				`
              &:hover {
                background: ${color.lightest};
              }
              &:active {
                box-shadow: rgba(0, 0, 0, 0.1) 0 0 0 3em inset;
              }
              &:focus {
                box-shadow: ${rgba(color.secondary, 0.4)} 0 1px 9px 2px;
              }
              &:focus:hover {
                box-shadow: ${rgba(color.secondary, 0.2)} 0 8px 18px 0px;
              }
          `}
      `}

      ${(props) =>
			props.appearance === APPEARANCES.inverseOutline &&
			`
          box-shadow: ${color.lightest} 0 0 0 1px inset;
          color: ${color.lightest};

          &:hover {
            box-shadow: ${color.lightest} 0 0 0 1px inset;
            background: transparent;
          }

          &:active {
            background: ${color.lightest};
            box-shadow: ${color.lightest} 0 0 0 1px inset;
            color: ${color.darkest};
          }
          &:focus {
            box-shadow: ${color.lightest} 0 0 0 1px inset,
              ${rgba(color.darkest, 0.4)} 0 1px 9px 2px;
          }
          &:focus:hover {
            box-shadow: ${color.lightest} 0 0 0 1px inset,
              ${rgba(color.darkest, 0.2)} 0 8px 18px 0px;
          }
      `};

`;
```

使用组件对其渲染：

```typescript
function Button(props: PropsWithChildren<ButtonProps>) {
	const { isLoading, loadingText, isLink, children } = props;
	const buttonInner = (
		<>
			<Text>{children}</Text>
			{isLoading && <Loading>{loadingText || "Loading..."}</Loading>}
		</>
	);
	const btnType = useMemo(() => {
		if (isLink) {
			return "a";
		}
	}, [isLink]);

	return (
		<StyledButton as={btnType} {...props}>
			{buttonInner}
		</StyledButton>
	);
}

```

同时定义默认传参与导出：

```typescript
Button.defaultProps = {
	isLoading: false,
	loadingText: null,
	isLink: false,
	appearance: APPEARANCES.tertiary,
	isDisabled: false,
	isUnclickable: false,
	containsIcon: false,
	size: SIZES.medium,
	ButtonWrapper: undefined,
};

export default Button;
```

这样Button组件就基本完成了。

## 编写STORY

### 设置PROPS TABLE

在编写story之前，我们需要对props table进行设置，props table是docs插件借助dockgen产生的，前面编写好Button组件后，自动会出来一堆属性，这些属性有很多是不用展示的，不需要全部列举出来。所以我们需要对属性进行过滤。

修改docgen插件配置：

.storybook\main.js

```javascript
{
	loader: require.resolve("react-docgen-typescript-loader"),
	options: {
		shouldExtractLiteralValuesFromEnum: true,
		propFilter: (prop) => {
			if (prop.parent) {
				return !prop.parent.fileName.includes(
					"node_modules"
				);
			}
			return true;
		},
	},
},
```

如果你的propFilter不起作用，请更换为2.1.1重新安装。

```
"@storybook/preset-create-react-app": "^2.1.1",
```

并显式安装react-docgen-typescript-loader。

```js
npm i react-docgen-typescript-loader -D
```

此时,propTable已经可以正常显示。



### 结合KNOB

Storybook强大地方就在于各种神奇插件，knob就是个非常好用的插件。

使用knob，我们可以在canvas里对组件进行互动，告别只能看设置好的示例。测试人员或者UI都可以借助平台对其进行调校，直到做出满意的组件。

我们使用CSF制作第一个knobstory:

src\components\button\button.stories.tsx

```typescript
import React from "react";
import Button, {
	APPEARANCES,
	AppearancesTypes,
	SIZES,
	SizesTypes,
} from "./index";
import {
	withKnobs,
	text,
	boolean,
	select,
} from "@storybook/addon-knobs";
export default {
	title: "Button",
	component: Button,
	decorators: [withKnobs],
};

export const knobsBtn = () => (
		<Button
			size={select<SizesTypes>("size", SIZES, SIZES.medium)}
			href={text("hrefText", "")}
			isLink={boolean("isLink", false)}
			loadingText={text("loadingTEXT", "I AM LOADING")}
			isLoading={boolean("isLoading", false)}
			disabled={boolean("disabled", false)}
			appearance={select<AppearancesTypes>(
				"APPEARANCES",
				APPEARANCES,
				APPEARANCES.primary
			)}
		>
			{text("childrenText", "Hello Storybook")}
		</Button>
);
```

打开storybook，canvas里可以进行各种调校即成功。

剩下的就是把各种设置做成story展示下即可：

```typescript
export const buttons = () => (
	<>
		<Button appearance="primary">Primary</Button>
		<Button appearance="secondary">Secondary</Button>
		<Button appearance="tertiary">Tertiary</Button>
		<Button appearance="outline">Outline</Button>
		<Button appearance="primaryOutline">Outline primary</Button>
		<Button appearance="secondaryOutline">Outline secondary</Button>
		<div style={{ background: "grey", display: "inline-block" }}>
			<Button appearance="inversePrimary">Primary inverse</Button>
		</div>
		<div style={{ background: "grey", display: "inline-block" }}>
			<Button appearance="inverseSecondary">Secondary inverse</Button>
		</div>
		<div style={{ background: "grey", display: "inline-block" }}>
			<Button appearance="inverseOutline">Outline inverse</Button>
		</div>
	</>
);

export const sizes = () => (
	<>
		<Button appearance="primary">Default</Button>
		<Button appearance="primary" size="small">
			Small
		</Button>
	</>
);

export const loading = () => (
	<>
		<Button appearance="primary" isLoading>
			Primary
		</Button>
		<Button appearance="secondary" isLoading>
			Secondary
		</Button>
		<Button appearance="tertiary" isLoading>
			Tertiary
		</Button>
		<Button appearance="outline" isLoading>
			Outline
		</Button>
		<Button appearance="outline" isLoading loadingText="Custom...">
			Outline
		</Button>
	</>
);

export const disabled = () => (
	<>
		<Button appearance="primary" disabled>
			Primary
		</Button>
		<Button appearance="secondary" disabled>
			Secondary
		</Button>
		<Button appearance="tertiary" disabled>
			Tertiary
		</Button>
		<Button appearance="outline" disabled>
			Outline
		</Button>
	</>
);

export const link = () => (
	<>
		<Button appearance="primary" isLink href="/">
			Primary
		</Button>
		<Button appearance="secondary" isLink href="/">
			Secondary
		</Button>
		<Button appearance="tertiary" isLink href="/">
			Tertiary
		</Button>
		<Button appearance="outline" isLink href="/">
			Outline
		</Button>
	</>
);
```



## 编写测试

昨天大家已经完成了button组件以及其story，story通常是视觉检测，一般来说，代码检测的工作也不能省，如果后期对组件进行维护，这个组件被多个别的组件所依赖，没有代码测试就很容易让某些组件的功能失效。导致左边补个bug，又边又多个bug出来。

### JEST

由于我们使用CRA进行安装，内部自带了JEST测试框架，我们使用JEST编写测试用例。

官网地址https://jestjs.io/

testing-library地址：https://testing-library.com/docs/react-testing-library/api

首先先建个button.test.tsx，放入\__test__文件夹下

src\components\button\__test__\button.test.tsx

```typescript
import React from "react";
import { render } from "@testing-library/react";
import Button from "../index";

test("hello test", () => {
	const wrapper = render(<Button>yehuozhili</Button>);
	const element = wrapper.queryByText("yehuozhili");
	expect(element).toBeTruthy();
});
```

首先试试第一个例子，编写完成后，运行`npm run test` 自动进入watch状态，会检测测试用例通过情况。

如果通过，试试修改断言内容，按正常预期工作即为成功。

如果这个错 
![reportwebpackerror](http://img.zhufengpeixun.cn/reportwebpackerror.png)

可以在项目根目录建一个文件`.env`
然后输入
```js
SKIP_PREFLIGHT_CHECK=true
```

### 按功能进行编写

一般来说，编写测试用例是按照代码覆盖率来的，可以先试着对主要功能进行编写，再根据代码覆盖率进行完善。

先给组件加上 `data-testid="button"`

src\components\button\index.tsx
```diff
function Button(props: PropsWithChildren<ButtonProps>) {
  const { isLoading, loadingText, isLink, children } = props;
  const buttonInner = (
    <>
      <Text>{children}</Text>
      {isLoading && <Loading>{loadingText || "Loading..."}</Loading>}
    </>
  );
  const btnType = useMemo(() => {
    if (isLink) {
      return "a";
    }
  }, [isLink]);

  return (
+    <StyledButton as={btnType} {...props} data-testid="button">
      {buttonInner}
    </StyledButton>
  );
}
````

然后安装测试用例
```typescript
import React from "react";
import { render, cleanup } from "@testing-library/react";
import { fireEvent } from "@testing-library/dom";
import Button, { ButtonProps, btnPadding } from "../index";
import { color, typography } from "../../shared/styles";
const defaultProps = {
	onClick: jest.fn(),
	className: "testprops",
};
const testProps: ButtonProps = {
	appearance: "primary",
	size: "small",
	className: "testprops",
};
const disabledProps: ButtonProps = {
	disabled: true,
	onClick: jest.fn(),
};

describe("test Button component", () => {
	it("should render the correct default button", () => {
		const wrapper = render(<Button {...defaultProps}>hello</Button>);
		const ele = wrapper.getByTestId("button");
		expect(ele).toBeInTheDocument();
		//正确渲染文本
		const text = wrapper.getByText("hello");
		expect(text).toBeTruthy();
		//button标签
		expect(ele.tagName).toEqual("BUTTON");
		expect(ele).not.toHaveAttribute("isdisabled");
		expect(ele).not.toHaveAttribute("isLinked");
		//正常添加classname
		expect(
			ele
				.getAttribute("class")
				?.split(" ")
				.includes("testprops")
		).toEqual(true);
		//正常click
		fireEvent.click(ele);
		expect(defaultProps.onClick).toHaveBeenCalled();
		//span正常显示
		expect(ele.getElementsByTagName("span")).toBeTruthy();
		//正常默认属性
		expect(ele).toHaveStyle(`background:${color.tertiary}`);
		expect(ele).toHaveStyle(`color: ${color.darkest}`);
		//正常大小
		expect(ele).toHaveStyle(`padding: ${btnPadding.medium}`);
		expect(ele).toHaveStyle(`font-size:${typography.size.s2}px`);
	});
	it("should render correct appearance ", () => {
		let wrapper = render(<Button {...testProps}>hello</Button>);
		let ele = wrapper.getByTestId("button");
		expect(ele).toHaveStyle(`background:${color.primary}`);
		expect(ele).toHaveStyle(`color: ${color.lightest}`);
		cleanup();
		wrapper = render(<Button appearance="inverseOutline">hello</Button>);
		ele = wrapper.getByTestId("button");
		expect(ele).toHaveStyle(
			`box-shadow: ${color.lightest} 0 0 0 1px inset`
		);
		expect(ele).toHaveStyle(`color: ${color.lightest}`);
		cleanup();
		wrapper = render(<Button appearance="inversePrimary">hello</Button>);
		ele = wrapper.getByTestId("button");
		expect(ele).toHaveStyle(`background:${color.lightest}`);
		expect(ele).toHaveStyle(`color: ${color.primary}`);
		cleanup();
		wrapper = render(<Button appearance="inverseSecondary">hello</Button>);
		ele = wrapper.getByTestId("button");
		expect(ele).toHaveStyle(`background:${color.lightest}`);
		expect(ele).toHaveStyle(`color: ${color.secondary}`);
		cleanup();
		wrapper = render(<Button appearance="outline">hello</Button>);
		ele = wrapper.getByTestId("button");
		expect(ele).toHaveStyle(`background:transparent`);
		expect(ele).toHaveStyle(`color: ${color.dark}`);
		cleanup();
		wrapper = render(<Button appearance="primaryOutline">hello</Button>);
		ele = wrapper.getByTestId("button");
		expect(ele).toHaveStyle(`box-shadow: ${color.primary} 0 0 0 1px inset`);
		expect(ele).toHaveStyle(`color: ${color.primary}`);
		cleanup();
		wrapper = render(<Button appearance="secondary">hello</Button>);
		ele = wrapper.getByTestId("button");
		expect(ele).toHaveStyle(`background:${color.secondary}`);
		expect(ele).toHaveStyle(`color: ${color.lightest}`);
		cleanup();
		wrapper = render(<Button appearance="secondaryOutline">hello</Button>);
		ele = wrapper.getByTestId("button");
		expect(ele).toHaveStyle(
			`box-shadow: ${color.secondary} 0 0 0 1px inset`
		);
		expect(ele).toHaveStyle(`color: ${color.secondary}`);
	});
	it("should render correct size ", () => {
		let wrapper = render(<Button {...testProps}>hello</Button>);
		const ele = wrapper.getByTestId("button");
		expect(ele).toHaveStyle(`padding: ${btnPadding.small}`);
		expect(ele).toHaveStyle(`font-size:${typography.size.s1}px`);
	});
	it("should render a link", () => {
		const wrapper = render(
			<Button isLink href="/">
				linkbutton
			</Button>
		);
		const ele = wrapper.getByTestId("button");
		expect(ele).toBeInTheDocument();
		expect(ele.tagName).toEqual("A");
		expect(ele).toHaveAttribute("href", "/");
	});
	it("should render disabled ", () => {
		const wrapper = render(<Button {...disabledProps}>hello</Button>);
		const ele = wrapper.getByTestId("button");
		expect(ele).toBeInTheDocument();
		expect(ele).toHaveStyle("cursor: not-allowed");
		fireEvent.click(ele);
		expect(disabledProps.onClick).not.toHaveBeenCalled();
	});
	it("should render loading ", () => {
		const wrapper = render(<Button isLoading>hello</Button>);
		const ele = wrapper.getByTestId("button");
		expect(ele).toBeInTheDocument();
		expect(ele).toHaveStyle("cursor: progress");
		const text = wrapper.getByText("hello");
		expect(text).toHaveStyle("opacity: 0");
		const wrapper2 = render(
			<Button isLoading loadingText="yehuozhili">
				hello
			</Button>
		);
		const text2 = wrapper2.getByText("yehuozhili");
		expect(text2).toBeTruthy();
	});
	it("should isUnclickable ", () => {
		const wrapper = render(<Button isUnclickable>hello</Button>);
		const ele = wrapper.getByTestId("button");
		expect(ele).toBeInTheDocument();
		expect(ele).toHaveStyle("pointer-events: none");
		fireEvent.click(ele);
		expect(disabledProps.onClick).not.toHaveBeenCalled();
	});
});


```

请根据自己组件特性完善测试。
关于fireEvent,如果版本大于9.5.0
```
"@testing-library/react": "^9.5.0"
```
可以用
```js
import { render,fireEvent, cleanup } from "@testing-library/react";
```
如果是如果版本是9.3.2
```js
    "@testing-library/react": "^9.3.2",
```
使用

```js
import { fireEvent } from "@testing-library/dom";
```

如果报错`@testing-library/dom`找不到类型声明文件可以添加
src\react-app-env.d.ts
```diff
/// <reference types="react-scripts" />
+declare module '@testing-library/dom'
```


## 快照测试

除了前面的测试方法，jest还提供快照测试。

如果你想的话，除了对test文件执行快照，还可以对story进行快照，以此来测试story文件中的变动。

story的快照需要用到storyShots插件，官网地址：https://github.com/storybookjs/storybook/tree/master/addons/storyshots/storyshots-core

story的快照这里不进行展开，对普通测试文件的快照可以直接调用：

```js
expect(wrapper).toMatchSnapshot();
```

我们在每个测试用例下可以进行快照，不同状态下的wrapper，生成的快照也不一样。

这样在文件改动后生成的快照不一样会让你进行检查，使用jest的命令模式w详细操作。



## 代码覆盖率

jest框架已经自带了代码覆盖率工具，不需要额外去安装nyc istanbul之类的工具了。

在package.json里添加scripts:

```
	"coverage": "react-scripts test --coverage --watchAll=false",
```

`npm run coverage` 即可看见代码覆盖率。

其中button.stores.tsx、global.tsx，以及snapshot覆盖率是0。

story文件我们不需要测它，snap文件不需要包含进覆盖率，通过package.json配置jest字段将其排除。

```
"jest": {
	"collectCoverageFrom": [
		"!src/**/*.stories.tsx",
		"src/**/*.tsx",
		"!src/**/*.snap"
	]
}
```

同时编写`globalStyle`的测试，`styles`一般不修改，直接给个快照完事。

src\components\shared\__test__\styles.test.tsx
```javascript
import React from "react";
import { render } from "@testing-library/react";
import { GlobalStyle } from "../global";

describe("test global style", () => {
	it("should render the correct global style", () => {
		const wrapper = render(<GlobalStyle></GlobalStyle>);
		expect(wrapper).toMatchSnapshot();
	});
});
```

再次进行覆盖率测试，覆盖率已经100%，如果有漏的可以根据行号写出对应的测试，这里我就不写了。

![testsuit100](http://img.zhufengpeixun.cn/testsuit100.png)

如果执行`npm run coverage`命令报错,可以修改

src\components\button\index.tsx
```diff
function Button(props: PropsWithChildren<ButtonProps>) {
  const { isLoading, loadingText, isLink, children } = props;
  const buttonInner = (
    <>
      <Text>{children}</Text>
      {isLoading && <Loading>{loadingText || "Loading..."}</Loading>}
    </>
  );
  const btnType = useMemo(() => {
    if (isLink) {
      return "a";
    }
  }, [isLink]);
  
-  return (
-    <StyledButton as={btnType} {...props} data-testid="button">
-      {buttonInner}
-    </StyledButton>
-  ); 
+   return React.createElement(StyledButton, { as: btnType, ...props ,"data-testid":"button"},buttonInner);
}
```

如果想看更详细内容，对coverage文件夹浏览即可。

```
npx http-server ./coverage
```

## 今日作业

完成button组件